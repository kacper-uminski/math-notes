#let book_template(
    title: none,
    course: none,
    author: "Kacper Uminski",
    doc
) = {
    set text(size: 10pt, font: "New Computer Modern")
    set outline(depth: 2)
    set math.equation(supplement: it => [Eq.#it])
    show outline.entry.where(level : 1): it => {
	v(12pt, weak: true)
	strong(it)
    }
    show heading.where(level: 1): it => {
	set text(size: 25pt)
	pagebreak(weak: true)
	it
	v(1cm)
    }
    set heading(
	numbering: (..nums) => {
	    let (section, ..subsections) = nums.pos()
	    if subsections.len() == 0 {
		[Chapter #section: ]
	    } else {
		numbering("1.1", ..nums.pos())
	    }
	},
    )
    show math.integral: math.limits.with(inline: false)
    align(center, text(18pt)[
	#course - #title
    ])
    show sym.emptyset: sym.phi.alt

    align(center, text(15pt)[
	#smallcaps(author)
    ])

    pagebreak()
    

    doc
}

#let titled_block(title, txt) = [
    #counter(heading).step(level: 3)

    *#title #context numbering("1.", ..counter(heading).get())* 
    #txt
]

#let example(txt) = titled_block("Example", emph(txt))

#let definition(txt) = titled_block("Definition", txt)



#let ncr(all, choice) = $vec(all,choice)$
