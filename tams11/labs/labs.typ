#import "@preview/statastic:0.1.0": *

#set text(size: 10pt, font: "New Computer Modern")
#set enum(numbering: "1)")

*Answer Sheet* Write down your name and personal number.

Kacper Uminski - 020427-7370

#let freq = csv("frequencies.csv", row-type: dictionary)
#let sample = csv("sample.csv").slice(1).flatten().map(int)
=== Problem 1
Frequencies are #freq.map(it => $f_#it.at("throw") = #it.at("Freq")$).join(", ")

#let mean = arrayAvg(sample)
#let theoretical_mean = range(1,7).sum()/6
Sample mean $overline(x) = #calc.round(mean, digits: 2)$
theoretical mean $E(X_i) = #theoretical_mean$

#let theoretical_std = calc.sqrt(range(1,7).map(it => calc.pow(it - mean, 2)).sum()/6)
Standard deviation $s = #calc.round(arrayStd(sample), digits: 2)$
theoretical standard deviation $sigma = #calc.round(theoretical_std, digits: 2)$

=== Problem 2
