#let math_template(
    title: none,
    course: none,
    doc
) = {
    set text(size: 12pt, font: "New Computer Modern")
    set heading(numbering: "1.")
    show math.integral: math.limits.with(inline: false)

    align(center, text(18pt)[
	#course - #title
    ])

    align(center, text(15pt)[
	Kacper Uminski
    ])

    pagebreak()
    

    doc
}

#let titled_block(title, txt) = align(center,block(
    width: 90%,
    fill: luma(230),
    inset: 8pt,
    radius: 4pt,
    align(left)[
	*#title*

	#txt
    ]
))

#let ncr(all, choice) = $vec(all,choice)$
