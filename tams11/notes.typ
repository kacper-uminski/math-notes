#import "@preview/cetz:0.2.2"
#import "@preview/fletcher:0.4.2" as fletcher: node, edge
#import "book.typ": *


#show: doc => book_template(
    title: "A Concise Course in Probability and Statistics",
    author: [Xiangfeng Yang #footnote[
	Department of Mathematics, Linköping University,
	#link("https://liu.se/en/employee/xiaza52")
    ]],
    course: "TAMS11",
    doc)
    
    #set page(numbering: "i")
    #heading(numbering: none)[Preface]

One aim is to limit the book within 100 pages in order to keep the book as
concise as possible, therefore many proofs are referred to references and
thus omitted.
    
    ????all problems with solutions!!!???

#pagebreak(weak: true)

#outline()

#set page(numbering: "1")
#counter(page).update(1)

= Basics on probability
  In this chapter, we introduce the (intuitive) definition of “probability” and
  its several computation techniques. To this end, we first give the definitions
  of sample spaces, events and their operations.

  == Sample spaces and events
    === Definition <def1.1.1>
      The _Sample space_ (usually denoted as $S$) of an experiment (or a trial)
      is the set of all possible outcomes of the experiment.
    === Example <eg1.1.2>
      _Now let us do an experiment: throw a fair die and observe the upper side.
      The sample space is $ S = {1,2,3,4,5,6} $_
    === Example <eg1.1.3>
      _Let us do another experiment: throw two fair dice, one by one, and
      observe the two sides. The sample space is
      $ S = {(1,1),...,(1,6); (2,1),...,(2,6);(6,1),...,(6,6)} $_
    === Definition <def1.1.4>
      An _event_ (usually denoted as $A, B, C, ...$)

    === Example <eg1.1.5>
      _In @eg1.1.2[Example], one can define three events as follows
      $ A = {2,4}, #h(12pt) B = {2,3}, #h(12pt)
	  C = {"Even numbers"} = {2,4,6} $_
    === Example <eg1.1.6>
      _In @eg1.1.3[Example], two events can be defined as
      $ D = {(5,6)}, #h(12pt)
	  E = {"Sum of two upper sides is 6"} =
	  {(1,5),(5,1),(2,4),(4,2),(3,3)} $_

      There are two special events: the sample space $S$ and the empty set
      $emptyset$. The fact that the sample space is an event is clear since it
      can be regarded as a subset of itself ($S subset.eq S$). The empty event
      $emptyset$ simply means that there are no elements inside. For instance,
      in @eg1.1.3[Example], if we define an event
      $F = {"Sum of two upper sides is 20"}$, then $F = emptyset$ as there is
      no element in the sample space for which the sum is 20.

      A very useful way to visually see a sample space and its events is as a
      _Venn diagram_ in which a rectangle represents the sample space $S$ and
      circles represent the events:

      #figure(caption: [Venn diagram],
	  align(center)[
	      #cetz.canvas({
		  import cetz.draw: *
		  rect((-3,-2), (3,2))
		  content((2.8,1.8), [S])
		  circle((-0.8,0), radius: 1.5)
		  content((-0.8,0), [A])
		  circle((1.2,0), radius: 1)
		  content((1.2,0), [B])
	      })
	  ]
      )

  == Operations on events
    For real numbers, there are four basic operations $+,-,times,div$. It is
    clear that these operations can not be applied to events, and in this
    section we introduce three basic operations on events.
    === Definition <def1.2.1>
      _Intersection_ of two events, $A$ and $B$, is denoted and defined as
      $ A sect B = {#[Elements in both $A$ and $B$]} $
    === Definition <def1.2.2>
      _Union_ of two events, $A$ and $B$, is denoted and defined as
      $ A union B = {#[Elements in at least one of $A$ and $b$]}$
    === Definition <def1.2.3>
      _Complement_ of an event $A$ is denoted and defied as
      $ A' = {#[Elements in S that are not in $A$]} $

      In @eg1.1.5[Example], it directly follows from the definitions that
      $A sect B = {2}, A union B = {2,3,4},$ and $A' = {1,3,5,6}$. It should be
      noted that Venn diagram can greatly help us to visually see and perform
      these operations:

      #align(center, grid(columns: (1fr, 1fr, 1fr),
	  [
	      #figure(caption: [$A sect B$],
		  cetz.canvas(length: 0.8cm, {
		      import cetz.draw: *
		      rect((-3,-2), (3,2))
		      content((2.8,1.8), [S])
		      circle((-0.8,0), radius: 1.5, name: "A", stroke: none)
		      content((-0.8,0), [A])
		      circle((1.2,0), radius: 1, name: "B", stroke: none)
		      content((1.2,0), [B])
		      intersections("i", "A", "B")
		      arc-through("i.0", "A.east", "i.1", mode: "CLOSE", stroke: gray, fill: gray)
		      arc-through("i.0", "B.west", "i.1", mode: "CLOSE", stroke: gray, fill: gray)
		      circle((-0.8,0), radius: 1.5)
		      circle((1.2,0), radius: 1)
		  })
	      )
	  ],
	  [
	      #figure(caption: [$A union B$],
		  cetz.canvas(length: 0.8cm, {
		      import cetz.draw: *
		      rect((-3,-2), (3,2))
		      content((2.8,1.8), [S])
		      circle((-0.8,0), radius: 1.5, stroke: none, fill: gray)
		      content((-0.8,0), [A])
		      circle((1.2,0), radius: 1, fill: gray)
		      circle((-0.8,0), radius: 1.5)
		      content((1.2,0), [B])
		  })
	      )
	  ],
	  [
	      #figure(caption: [$A'$],
		  cetz.canvas(
		      length: 0.8cm,
		      background: gray,
		      {
			  import cetz.draw: *
			  rect((-3,-2), (3,2))
			  content((2.8,1.8), [S])
			  circle((-0.8,0), radius: 1.5, fill: white)
			  content((-0.8,0), [A])
		      }
		  )
	      )
	  ],
      ))

    === Definition <def1.2.4>
    Two events, $A$ and $B$, are _disjoint_ if $A sect B = emptyset$

    === Theorem <th1.2.5>
      _It holds that
      $(A sect B)' = A' union B'$ and
      $(A union B)' = A' sect B'$._

      The proof of @th1.2.5[Theorem] can be seen by drawing a Venn diagram for
      each side of the equality.

  == Probabilities of events
    === The (intuitive) definition of "probability"
      An essential concept in the theory of probability theory is “probability
      of an event” which is written as $P(A)$ for an event $A$. The very first
      task is to define $P(A)$. However, to rigorously define a probability, one
      needs to recall Kolmogorov Axioms (see @Chernoff1954)#footnote[
	  The triple $(S, cal(F), P)$ is said to be a probability space if $P$
	  satisfies the Kolmogorov Axioms:
	  #set enum(numbering: "(i)")
	  + $P(A) >= 0$ for any event $A in cal(F)$

	  + $P(S) = 1$

	  + $P(union.big_(n=1)^oo A_n) = sum_(n=1)^oo P(A_n)$ for disjoint
	    events $A_1, A_2, ...$

	  In this setting, $S$ is called the sample space, $cal(F)$ is the
	  collection of events and $P$ is a probability.
      ]. But for an undergraduate course, let us just intuitively understand a
      probability as a chance. Even in this intuitive setting, some remarks seem
      to be necessary. If one looks at an experiment of tossing a fair coin,
      then one usually understands the word “fair” as the 50% chance of getting
      a head (and of course the 50% chance of getting a tail). However this 50%
      chance should be understood as follows: one tosses a fair coin $n$ times
      and counts the relative frequencies of getting heads, then it is shown
      that the limit of these relative frequencies as $n$ tends to infinity is
      50%. Note that for each fixed $n$, it can only be said that the relative
      frequency of getting heads is close (possibly not exactly equal) to 50%.
      
    === Computation techniques of probabilities
      #[
	  #set enum(full: true, numbering: (..n) => {
	      let format = if n.pos().len() > 1 {
		  "(a)"
	      } else {
		  it => strong(numbering("(I)", it))
	      }
	      numbering(format, n.pos().last())
	  })

	  + *Rules*
	      + $P(S) = 1, P(emptyset) = 0$;
	      + $P(A') = 1-P(A)$;
	      + $P(union.big_(i=1)^n A_i) = sum_(i=1)^n P(A_i)$ for disjoint
	        events $A_1,...,A_n$;
	      + $P(A union B) = P(A) + P(B) - P(A sect B)$, for any events $A$
	        and $B$;
	      + For any events $A$, $B$, and $C$,
	        $ P(A union B union C) = P(A)+P(B)+P(C)-P(A sect B)-P(A sect C)
		    -P(B sect C)+P(A sect B sect C) $
	    All of these rules can be directly seen from Venn diagrams.

	  + *An essential formula* Assume that outcomes of an experiment are
 	    equally likely, then
	    #math.equation(
	        numbering: "(1)",
		supplement: "Eq.",
		block: true)[$ P(A) = N(A)/N $ ] <eq1>
	    where $N(A)$ represents the number of elements in $A$ (alternatively
	    the number of different ways to achieve $A$), and $N$ is the number
	    of total outcomes (alternatively the number of different ways in
	    total.) Sometimes it is very easy to count $N(A)$ and $N$, and below
	    is such an example.
      ]
      
    === Example <eg1.3.3>
      _If we throw a fair die and observe the upper side, then the outcomes of
      this experiment are equally likely. Define an event $A = {2,4}$. It follows
      from the essential formula (@eq1) that
      $ P(A) = N(A)/N = 2/6 = 1/3 $_

      However, sometimes it is not easy to count $N(A)$ and $N$, and we need to
      use suitable counting techniques. Below we include three such techniques.

      / Product: #[
	  Suppose that for a counting there are $k$ steps, (or places): #[
	      #set box(stroke: 0.5pt, height: 11pt, baseline: 20%)
	      #box[$n_1$]#box[$n_2$]...#box[$n_k$]
          ], and in the $i$-th step there are $n_i$ different elements (or
	  ways.) Then, in total, there are $product_(i=1)^k n_i$ different
	  elements (or ways.)
      ]

      / Combination: #[
	  Suppose that there are $n$ different objects from which one
          wants to choose $k$ different objects. Then the number of ways to
	  choose is $C_(k,n) = vec(n,k) = n!/(k!(n-k)!)$. It should be noted
	  that for these $k$ chosen objects, the order is not important.
      ]

      / Permutation: #[
	  Suppose that there are $n$ different objects, and now one wants to
	  choose $k$ different objects and then arrange them according to an
	  order. Then the number of different ways is $P_(k,n) = n!/(n−k)!$.
	  Here for these $k$ chosen objects, the order is important!
      ]

      We can now apply these counting techniques to one specific example.

    === Example <eg1.3.3>
      _A group of 60 students will have a party, and 2 of these students will be
      randomly selected to buy beers and snacks for the party. Suppose that in
      this group there is (only) one student named David Beckham. Now define an
      event $A = {"David Beckham is selected"}$. What is the probability $P(A)$?
      We will answer this question using two different approaches._

      _Solution:_
      #enum(numbering: "(i)", tight: false)[
	  The first approach is based on combinations. If we think that these 2
	  students always go together, then the order in which they are selected
	  is not important (namely we can think of selecting these 2 students at
	  once), therefore the essential formula (@eq1) implies
	  $ P(A) = N(A)/N = (1 dot 59)/C_(2,60) = 1/30 $
	  The counting of $N(A)$ uses the product technique since there are two
	  steps: step one is to select David (for which there is only 1 way) and
	  step two is to select someone else (there are 59 ways). Note that here
	  the order does not matter since these 2 students are considered as a
	  group. The counting of $N$ here follows directly from the combination.
      ][
	  The second approach is based on permutations. If we now think that
	  these 2 students do not go together (one goes to buy beers and one
	  goes to buy snacks), then the order really matters (imagine that David
	  and Lisa are selected, then it could happen that David buys beers and
	  Lisa buys snacks, or vice versa). Keeping this in mind, the essential
	  formula (@eq1) implies
	  $ P(A) = N(A)/N = (1 dot 59 + 59 dot 1)/P_(2,60) = 1/30 $
      ]

  == Conditional probabilities, total probabilities and Bayes’ Theorem
    In this section we introduce three more techniques to compute probabilities
    (techniques involving conditional probabilities, total probabilities and
    Bayes’ Theorem), together with two approaches (the tree approach and the
    table approach.)     

    === Conditional probabilities
      Given two events, $A$ and $B$ with $P(B) > 0$, the _conditional
      probability_ of $A$ given $B$, written as $P(A|B)$, is defined as
      $ P(A|B) = P(A sect B)/P(B) $
      How to intuitively understand a conditional probability: just regard the
      condition $B$ as a new sample space! For instance, if we throw a fair die
      and set $A = {2}$ and $B = {2,3}$, then the conditional probability
      $P(A|B) = 1/2$. With the condition $B = {2, 3}$, one simply thinks that
      the die can only show 2 or 3, therefore the probability of getting
      $A = {2}$ is $1/2$. The main motivation for introducing conditional
      probabilities is the fact that for many applications, suitable
      conditions can only be expressed as conditional probabilities which will
      be partially explained in the following example.

    === Example <eg1.4.2>
      _Suppose that a factory makes items (such as computers.) Before these
      items are sold they have to pass the quality control established by this
      factory. Assume that the defective rate of these items is 5% (which means
      that 5% of all the items that the factory makes are defective). Further
      assume that a defective item can pass the quality control at a rate of 1%
      and that good items always pass. What is the probability that a randomly
      selected item is defective and can pass the quality control?_
    
      _Solution:_ Define the following events:
      $ A & = {"A randomly selected item is defective"} \
	  B & = {"A randomly selected item can pass"} $
      The problem asks for the probability of $P(A sect B)$ with the following
      being given:
      $ P(A) & = 5%, #h(12pt) P(B|A) & = 1%, #h(12pt) P(B|A') & = 100% $
      We can proceed in three ways:

      #enum(numbering: "(i)", tight: false)[
	  It follows directly from the definition of conditional probabilities
	  that $ P(A sect B) = P(B|A) dot P(A) = 1% dot 5% = 0.05% $
      ][
	  We draw a tree with two levels. Level 1 specifies whether or not an
	  item is defective, and level 2 specifies whether or not an item can
	  pass through quality control:
	  #align(center,
	      cetz.canvas(({
		  import cetz.tree
		  import cetz.draw: *
		  let data = ([A randomly selected item],
		      ([$A$ (defective, 5%)],
			  ([$B$ (pass, 1%)], ([$ P(A sect B) \ = 5% dot 1% $])),
			  ([$B'$ (fail, 99%)], ([$ P(A sect B') \ = 5% dot 99% $]))),
		      ([$A'$ (good, 95%)],
			  ([$B$ (pass, 100%)], ([$ P(A' sect B) \ = 95% dot 100% $])),
			  ([$B'$ (fail, 0%)], ([$ P(A' sect B') \ = 95% dot 0% $]))))
		  cetz.tree.tree(
		      data,
		      draw-node: (node, ..) => {
			  content((), text([#block(
			      inset: 4pt,
			      radius: 3pt,
			      stroke: 0.5pt,
			      node.content)]))
		      },
		      spread: 4,
		      grow: 1.2
		  )
	      })))
      ][
	  We take 10 000 such items and classify them as follows:
	  #align(center,table(columns: 3,
	      [],[$A$ (defective, 500 items)],[$A'$ (good, 9500 items)],
	      [$B$ (pass)],[5],[9500],
	      [$B'$ (fail)],[495],[0]
	  ))
	  It then directly follows from the table (and formula @eq1) that
	  $P(A sect B) = 5/10000 #h(12pt) qed$ 
      ]

    === Total Probabilities
      If ${A_i}_(i=1)^k$ is a separation of the sample space $S$ (namely
      $A_i$ are disjoint $forall i$ and the union $union.big_(i=1)^k A_i = S$)
      then the _total probability formula_ is
      #math.equation(block: true, numbering: "(1)")[
	  $P(B) = sum_(i=1)^k P(B|A_i) dot P(A_i)$
      ] <eq2>
      Loosely speaking, the total probability $P(B)$ can be split into $k$
      smaller probabilities. This begs the question of why we would want to do
      split a probability. This is answered in the following example.
    === Example <eg1.4.4>
      _In @eg1.4.2[Example], what is the probability that a randomly selected
      item can pass the quality control?_

      _Solution:_
      #enum(numbering: "(i)")[
	  The first approach uses total probability. We want to find $P(B)$,
	  however $P(B)$ is not given directly. Instead, two conditional
	  probabilities are given, $P(B|A) = 1%$ and $P(B|A') = 100%$.
	  Therefore a splitting is desired and the total probability formula can
	  be used:
	  $ P(B) = P(B|A) dot P(A) + P(B|A') dot P(A')
	      = 1% dot 5% + 100% dot 95%
	      = 95.05% $
      ][
	  The second approach uses the table from @eg1.4.2[Example] (Approach 3
	  therein). From the table and @eq1 it directly follows that
	  $P(B) = (5 + 9500)/10000 = 95.05% #h(12pt) qed $
      ]

    === Bayes' Theorem
      If two events, $A$ and $B$, both have positive probabilities, $P(A) > 0$
      and $P(B) > 0$ then _Bayes' Theorem_ states that
      $ P(A|B) = (P(B|A) dot P(A))/P(B) $
      namely, the positions of $A$ and $B$ can be reversed in this way. See the
      following example for why this can be necessary.
    === Example <eg1.4.6>
      _In @eg1.4.2[Example], given that a randomly selected item passes the
      quality control, what is the probability that this item is defective?
      (This is a basic problem in control theory.)_

      _Solution:_
      #enum(numbering: "(i)")[
	  Approach 1 uses Bayes' theorem. What needs to be calculated is
	  $P(A|B)$. However, what we know is that $P(B|A) = 1%$, therefore we
	  want to switch the positions of $A$ and $B$. To this end, Bayes'
	  Theorem implies that
	  $ P(A|B) = (P(B|A) dot P(A))/P(B)
	      = (1% dot 5%)/underbrace(95.05%, #[@eg1.4.4[Example]])
	      = 0.0526% $
      ][
	  Approach 2 once again uses the table from @eg1.4.2[Example] and @eq1.
	  These give that $ P(A|B) = 5/(5+9500) = 0.0526% #h(12pt) qed $
      ]


  == Independence of events
    === Definition <def1.5.1>
      Two events, $A$ and $B$, are said to be _independent_ if
      $P(A sect B) = P(A) dot P(B)$.

      Intuitively speaking, if $A$ and $B$ are independent, then the occurrence
      of $A$ does not affect the occurrence of $B$ and vice versa. For instance,
      if we throw a fair die and set $A = {2}$, $B = {2, 3}$ and $C = S$ (the
      sample space), then one can see that $A$ and $B$ are NOT independent
      (because if $A$ happens, then $B$ will happen consequently), but $A$ and
      $C$ are independent (since the occurrence of $A$ will not at all affect
      the occurrence of S, which happens for sure). It should be noted that if
      $A$ and $B$ are independent, then $A′$ and $B$ are also independent (the
      same is true for the pairs $A$ and $B′$ as well as $A′$ and $B′$.)

    === Definition <def1.5.2>
      Events $A_1, A_2,...,A_n$ are said to be _independent_ if
      $forall {i_1, i_2, ..., i_k} subset.eq {1,2,...,n}$ it holds that
      $P(sect.big_(j=1)^k A_i_j) = product_(j=1)^k P(A_i_j)$

      One should notice the fact that in order to make events $A_1, A_2,...,A_n$
      independent, it is NOT enough to only require that
      $P(union.big_(i=1)^n A_i) = product_(i=1)^n P(A_i)$, since it can not
      be deduced from this equality that $A_i$ and $A_j$ are independent
      $forall i,j$.

    === Example <eg1.5.3>
      #enum(numbering: "(i)")[
	  _(Serial system) Suppose that a signal is transmitted from a to b
	  through a serial system with four independent stations,
	  $(T_i)_(i=1)^4$ as follows:_

	  #align(center,
	      fletcher.diagram(
		  cell-size: 5mm,
		  $
		      a edge("r", ->)
			  & T_1 edge("r", ->)
			  & T_2 edge("r", ->)
			  & T_3 edge("r", ->)
			  & T_4 edge("r", ->)
			  & b
		  $))
	  _Suppose that the successful transmission probability of each station
	  is $P(T_i) = 90%, 1 <= i <= 4$. What is the probability the signal can
	  be successfully transmitted from $a$ to $b$_?

      ][
	  _(Parallel system) Suppose that a signal is transmitted from $a$ to
	  $b$ through a parallel system with four independent stations
	  $T_i, 1 <= i <= 4$ as follows:_ 
	  #align(center,
	      fletcher.diagram(
		  cell-size: 5mm,
		  $
		      & & & & T_1 edge("ddrr") \
			  & & & & T_2 edge("drr") \
		      a edge("rr", ->) &&
		      edge("uurr", ->) edge("urr", ->) edge("drr", ->) edge("ddrr", ->)
			  & & & & edge("rr", ->) & & b \
			  & & & & T_3 edge("urr") \
			  & & & & T_4 edge("uurr")
		      $))
	  _Suppose that the successful transmission probability of each station
	  is $P(T_i) = 90%, 1 <= i <= 4$. What is the probability the signal can
	  be successfully transmitted from $a$ to $b$_?
      ]

      _Solution_
      #enum(numbering: "(i)")[
	  The probability is, based on the independence,
	  $ P(sect.big_(i=1)^4 T_i) = product_(i=1)^4 P(T_i) = (90%)^4 = 65.61% $
      ][
	  The probability is, based on the independence,
	  $ P(union.big_(i=1)^4 T_i) & = 1 - P((union.big_(i=1)^4 T_i)') \
	      & = 1 - P(sect.big_(i=1)^4 T_i) \
	      & = 1 - product_(i=1)^4 P(T'_i) \
	      & = 1 - (1 - 90%)^4 \
	      & = 99.99% $
      ]

= Random variables
  In this chapter we introduce the most basic concept in probability,
  _random variable_, which will be used throughout the book. Again, we will
  choose an intuitive definition of random variables, although a rigorous
  definition is given for completeness. After discussions on one-dimensional
  random variables, we also explore two-dimensional random variables (also
  called _random vectors_). Two essential concepts of a random variable, mean
  and variance, are also defined and explained.

  Throughout the book, any one-dimensional random variable is assumed to take
  values on the real line for the sake of simplicity.

  == One dimensional random variables
    
      
    #pagebreak(weak: true)
    #bibliography("sources.bib") 

      
