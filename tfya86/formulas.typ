#import "@preview/physica:0.9.3": *
#show math.phi: math.phi.alt
#set page(flipped: true)
#set text(size: 9pt, font: "New Computer Modern")
#set grid(gutter: 1em)
#grid(columns: (1fr, 1fr, 1fr))[
    *Elektromagnetism*
    #grid(columns: (1fr, 1fr),
	[Coulombs lag], $F_"el" = 1/(4pi epsilon_0)$,
	[Elektrisk fältstyrka], $va(E) = va(F)_0/q_0$,
	[Elektrisk fältstyrka omkring punktladdning], $va(E) = 1/(4pi epsilon_0) q/r^2 vu(r)$,
	[Elektriskt flöde], $Phi_E & = va(E) dot va(A)
	    \ & = E A cos phi
	    \ & = integral va(E) dot dd(va(A))
	    \ & = integral E cos phi dd(A)$,
	[Gauss lag för elektriskt fält], $Phi_E = integral.cont va(E) dot dd(va(A)) = Q_"encl"/epsilon_0$,
	[Potentiell energi i elektriskt fält omkring punktladdning (om $U = 0$ vid $r = oo$)], $U = (q q_0)/(4pi epsilon_0) 1/r$,
	[Elektrisk potential], $V = U/q_0$,
	[Elektrisk potential kring punktladdning (om $V_(r=oo) = 0$)], $V = q/(4pi epsilon_0 r)$,
	[Elektrisk potential], $-Delta V = integral_a^b va(E) dot dd(va(l))$,
	[Elektriskt fält], $va(E) = -va(grad)V$,
	[Vridmoment för elektrisk dipol], $va(tau) = va(p) times va(E)$,
	[Potentiell energi för elektrisk dipol], $U = -va(p) dot va(E)$,
	[Elektrisk fältstyrka för kondensator], $E = sigma/epsilon_0$,
	[Elektrisk fältstyrka för kondensator med dielektriskt material], $E = E_0/K$,
	[Energilagring i kondensator], $U = Q^2/(2C)$,
	[Energitäthet i elektriskt fält],$u = 1/2 epsilon_0 E^2$
    )
][
    *Magnetfält och magnetism*
    #grid(columns: (1fr, 1fr),
	[Kraft på laddad partikel i magnetfält], $va(F_B) = q va(v) times va(B)$,
	[Kraft på strömförande ledare], $dd(va(F)) = I dd(va(l)) times va(B)$,
	[Biot-Savarts lag], $va(B) = mu_0/(4pi) integral dd(Q (va(v) times va(r))/r^2)$,
	[Magnetiskt flöde], $Phi_B & = integral va(B) dd(va(A)) \ & = integral B cos phi dd(A)$,
	[Gauss lag för magnetiskt fält], $integral.cont va(B) dot dd(va(A)) = 0$,
	[Magnetfält på avstånd $x$ från rak ledare], $B = (mu_0 I)/(2pi x)$,
	[Amperes lag], $integral.cont va(B) dot dd(va(l)) = mu_0 I_"encl"$,
	[Kraft mellan två strömförande ledare], $F/L = (mu_0 I I')/(2pi r)$,
	[Dipolmoment (magnetisk dipol)], $va(mu) = I va(A)$,
	[Vridmoment för magnetisk dipol], $va(tau) = va(mu) times va(B)$,
	[Magnetfält i magnetiskt material i yttre fält $B_0$], $va(B) & = va(B_0) + mu_0 va(M) \ & = va(B_0) K_m $,
	[Relativ permeabilitet], [$K_m$],
	[Magnetiskt susceptibilitet], $chi_m = K_m - 1$
    )
][
    *Ström och induktion*
    #grid(columns: (1fr, 1fr),
	[Elektrisk ström], $I = dv(Q,t) = n q v_d A$,
	[Strömtäthet], $va(J) & = n q va(v_d) \ & = (n q^2 tau)/m va(E)$,
	[Strömtäthet], $|va(J)| & = I/A = n q v_d$,
	[Ohms lag], $E = rho J$,
	[Resistivitet], $rho = m/(n q^2 tau)$,
	[Temperaturberoende hos resistivitet], $rho(T) = rho_0 (1 + alpha(T - T_0))$,
	[Ohms lag om resistiviteten är konstant], $V = R I$,
	[Resistans hos ledare], $R = (rho L)/A$,
	[]
    )
]


