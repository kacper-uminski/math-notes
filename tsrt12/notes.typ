#import "@preview/fletcher:0.5.4" as fletcher: diagram, node, edge
#import "@preview/physica:0.9.4": *

#set heading(numbering: "1.")
#set text(font: "New Computer Modern")
#let example(name, txt) = align(
  center,
  box(fill: gray, inset: 1em, radius: 5pt, width: 80%)[
    #set align(left)
    *Exempel: #name*

    #txt
  ],
)

= P-reglering
$u(t) = K_P(r(t)-y(t))$
gre
= PI-reglering
Antag att styrsignalen beräknas som
$
  u(t) = K_P dot e(t) + K_I integral_0^t e(tau) dd(tau)
$
där $e(t) = r(t)-y(t)$. Notera att regulatorn nu har fått ett minne i form av en
integral. Då kan $u(t)$ beräknas baserat på gamla värden av $e(t)$. Detta kallas
_Proportionell och Integrerande reglering_ (PI). Två designparametrar: $K_P$
och $K_I$.
== I-deleen:
- Eliminerar stationära fel
- Gör systemet mer oscillativt.

= PID-reglering
Antag att styrsignalen beräknas som
$
  u(t) = K_P dot e(t) + K_I integral_0^t e(tau) dd(tau) + K_D dot dv(e(t),t)
$
där $e(t) = r(t)-y(t)$. Detta kallas för _Proportionell, Integrerande, och
Deriverande reglering_ (PID). Tre designparametrar: $K_P, K_I, K_D$.
== Tolkning av D-delen
Taylorutveckla reglerfelet kring tidpunkten $t$:
$
  e(t+tau_D) approx e(t) + Tau_D dot dv(e(t), t)
$
== PD-regulator:
$
  u(t) & = K_P dot e(t) K_D dv(e(t), t) \
  & = K_P(e(t) + K_D / K_P dot dv(e(t), t)) = {Tau_D = K_D / K_P} \
  & = K_P(e(t) Tau_D dot dv(e(t),t)) = {"Identifiera med" e(t+Tau_D)} \
  & = K_P dot e(t+Tau_D)
$
#sym.because Införandet av en D-del kan tolkas som att man återkopplar från
framtida reglerfel.

= Andra ordningens system med komplexa poler
Ett stabilt andra ordningens system med komplexkonjugerade poler kan skrivas
på formen:
$
  G(s) = omega_0^2 / (s^2+2zeta omega_0s+omega_0^2)
$
/ $omega_0$: Avståndet från pol till origo.
/ $Phi$: Vinkeln mellan polen och x-axeln.
/ $zeta$: $cos Phi = $ relativ dämpning

- Stort $omega_0$ ger ett snabbt system: $omega_0$ är en ren tidskalning av
  stegsvaret.
- Litet $zeta$ ger stor vinkel $Phi$, vilket i sin tur ger ett svängigt/dåligt
  dämpat system.

= Det slutna systemet
#diagram(cell-size: 15mm, $
  & & & & v "(Störning)" edge("d", +, "->") \
  edge(r+, "->")
  & Sigma edge(e, "->")
  & F(s) edge(u, "->")
  & G(s) edge(+, "->")
  & Sigma edge("->")
  & y \
  & edge("u", -, "->") edge() & edge() & edge() & edge("u")
$)

Detta ger:
$
  Y(s) = G(s)dot U(s) + V(s) = G(s)dot F(s) dot (R(s)-Y(s)) + V(s) <=> \
  <=> (1+G(s) dot F(s)) dot Y(s) = G(s) dot F(s) dot R(s) + V(s) <=> \
  <=> Y(s) = underbrace((G(s) dot F(s))/(1 + G(s) dot F(s)), G_c (s) (r -> y)) dot R(s) + underbrace(1/(1+G(s) dot F(s)), S(s) (v -> y)) dot V(s)
$

Dessutom:
$
  E(s) = R(s) - Y(s) = (1-G_c(S)) dot R(s) - S(s) dot V(s) = S(s) dot R(s) - S(s) dot V(s)
$

Kretsförstärkningen:
$
  G_o(S) = G(s) dot F(s)
$

En mer allmän linjär återkoppling:

#diagram(cell-size: 15mm, $
  & & & & v edge("d", +, "->") \
  edge(r, )
  &  F_r (s) edge(+, "->")
  & Sigma edge("->")
  & G(s) edge(+, "->")
  & Sigma edge("->")
  & y \
  & & edge("u", -, "->") edge() & F_y (s) edge("<-") & edge("u")
$)

- Här ges kretsförstärkningen av $G(s) = G(s) dot F_y (s)$.
- Notera att $G_o (s)$ förekommer både i $S$ och i $G_c$.
- Notera att $S(s)$ bara beror på $F_y (s)$ och inte på $F_r (s)$.
  Detta ger möjlighet att forma $S(s)$ och $G(s)$ oberoende av varandra.

Alternativt uttryck:

$
  Y(s) = underbrace((G(s) dot F_r (s))/(1+G(s) dot F_y (s)), G_c (s))
  + underbrace(1/(1+G(s) dot F_y (s)), S(s)) dot V(s)
$

Vidare kan man derivera $e_0$:

$
  & lim_(t->oo) e(t) \
  = & {"Slutvärdesteoremet"} \
  = & lim_(s->0) s dot E(s) \
  = & lim_(s->0) s dot S(s) dot R(s) \
  = & lim_(s->0) s dot 1/(1+G_o (s)) dot A/s \
  = & underbrace(1/(1+G_o (0)), e_0) dot A
$

samt $e_1$:
$
  & lim_(t->oo) e(t) \
  = & lim_(s->o) s dot E(s) \
  = & lim_(s->0) s dot 1/(1+1/s dot H(s)) dot A/S^2 \
  = & lim_(s->0) 1/(S+H(s)) dot A \
  = & underbrace(1/H(0), e_1) dot A
$

På så sätt blir $e_1 = 0$ om $H(0) = oo$, det vill säga även $H(s)$ innehåller
en integration.

#example("Polplanering")[
  - System att reglera: $G(s) = 1/(s+2)$

  - PI-regulator: $F(s) = K_p + K_i/s$

  $
    
    G_c (s) = & (G(s) dot F(s))/(1+G(s) dot F(s)) \
    = & (1/(s+2) dot (K_p dot s + K_I)/s)/(1+1/(s+2) dot (K_p dot s + K_I)/s) \
    = & (K_p dot s + K_I)/(s^2 + (2 + K_p) dot s + K_I)
  $

  - Önskade poler: $-3$ och $-4$

  - Detta svarar mot ett nämnarpolynom $(s+3)(s+4) = s^2 + 7s + 12$.

  - Alltså: Om vi väljer $2 + K_p = 7 <=> K_p = 5$ och $K_I = 12$ får $G_c (s)$
    de önskade polerna.

  - Intressant fråga: Hur kommer $G_c (s)$ poler att ändras om vi börjar skruva
    på $K_p$ eller $K_I$?
]

#example("Rotort")[
  $ underbrace((s+1)(s-2), P(s)) + K dot underbrace((s+2), Q(s)) = 0 $
  $ n = 2, m = 1 $
  - Startpunkter: $-1, 2$
  - Ändpunkt: $-2$
  - Asymptoter: $n-m=1$st, Riktning #math.pi

  (Se slide 20 på föreläsning 3.)

  Matlab:
  ```matlab
  rlocus(Q/P)
  ```
]
